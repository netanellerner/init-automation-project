package db.PersonalRequests;

import db.connection.DBConnection;
import framework.cleanup.CleanupService;
import lombok.SneakyThrows;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.List;

import static java.sql.Statement.RETURN_GENERATED_KEYS;

public class SomeDAL extends DBConnection {

    @SneakyThrows
    public static void findAll() {
        Connection connection = getConnection();

        PreparedStatement select =
                connection.prepareStatement("""
                        SELECT * FROM "Table"
                        """);

        List<SomeModel> request = map(select.executeQuery(), SomeModel.class);

        try {
            request.forEach(System.out::println);
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    public static SomeModel findOneById(Integer id) {
        Connection connection = getConnection();

        PreparedStatement select =
                connection.prepareStatement("""
                        SELECT * FROM "Table"
                        WHERE id = %d
                        """.formatted(id));

        List<SomeModel> request = map(select.executeQuery(), SomeModel.class);

        try {
        } finally {
            connection.close();
        }
        if (request.size() == 1) {
            return request.get(0);
        }
        if (request.isEmpty()) {
            return null;
        }
        throw new IllegalArgumentException("More then one result");
    }

    @SneakyThrows
    public static void findBySubType(Integer columnName) {
        Connection connection = getConnection();

        PreparedStatement select =
                connection.prepareStatement("""
                        SELECT * FROM "Table"
                        WHERE columnName = %d
                        """.formatted(columnName));

        List<SomeModel> request = map(select.executeQuery(), SomeModel.class);

        try {
            request.forEach(System.out::println);
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    public static void save(String column1, Integer column2) {
        Connection conn = getConnection();

        PreparedStatement statement = conn.prepareStatement("""
                INSERT INTO "Table"
                (
                columnName, 
                columnName
                )
                VALUES
                ('%s',
                %d
                );
                              """.formatted(
                column1
                , column2
        ), RETURN_GENERATED_KEYS);
        statement.execute();
//        ResultSet resultSet = statement.getGeneratedKeys();
//        resultSet.next();
//        int id = resultSet.getInt(1);
//        System.out.println(black_id);
        conn.close();

        CleanupService.stack(() -> deleteAll(""));
    }


    @SneakyThrows
    public static void deleteAll(String personalId) {

        Connection connection = getConnection();
        connection.prepareStatement("""
                DELETE FROM "Table"
                WHERE columnName = '%s'
                """.formatted(personalId)).execute();
        try {
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    public static void deleteById(Integer id) {
        Connection connection = getConnection();

        connection.prepareStatement("""
                DELETE FROM "Table"
                where columnName = %d
                       """.formatted(id)).execute();
        try {
        } finally {
            connection.close();
        }
    }
}
