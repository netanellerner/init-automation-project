package db.PersonalRequests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import db.connection.Column;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalDateTime;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@Data
@Accessors(fluent = true)
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(NON_NULL)
public class SomeModel {

    @Column("id")
    private Integer id;

    @Column("req_id")
    private String req_id;

    static SomeModel fakePersonalRequestBody(String personalNumber, Integer subType) throws ParseException {
        return new SomeModel()
                .req_id("0")
                .id(1);
    }

}
