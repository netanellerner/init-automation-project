package db.genericDAL;

import db.connection.DBConnection;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.ParameterizedType;
import java.util.List;

@Slf4j
public abstract class GenericDAL<Model, Key> extends DBConnection {
    protected final Class<Model> type;

    @SuppressWarnings("unchecked")
    protected GenericDAL() {
        this.type = (Class<Model>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0
                ];
    }

    public List<Model> findAll() {
        log.info("SELECT * FROM %s".formatted(type.getName()));
        return null;
    }

}
