package UI.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SomePage extends HomePage{

    @FindBy(xpath = "//*[text()='הגשת בקשה חדשה']")
    private WebElement newRequestButton;

    @FindBy(css = ".css-1qftfxl")
    private WebElement connectName;


    public SomePage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    public SomePage someFunction() throws InterruptedException {
        super.waitInnerHtmlIsDifferentThen(connectName, "להתחברות");
        super.waitInnerHtmlIsContainsToOrTo(connectName, "בוקר טוב,נתנאל","צהריים טובים,נתנאל");
        super.waitAndClick(newRequestButton);
        return this;
    }
}
