package UI.pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.html5.LocalStorage;
import org.openqa.selenium.html5.WebStorage;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;


public class PageObject {
    // D-M
    public WebDriver driver;
    public WebDriverWait wait;

    //Constructor
    public PageObject(WebDriver driver, WebDriverWait wait) {
        this.driver = driver;
        this.wait = wait;
        PageFactory.initElements(driver, this);
    }

    // Methods
    public void waitAndClick(WebElement button) {
        wait.until(elementToBeClickable(button)).click();
    }

    public void waitElementToBeClickable(WebElement button) {
        wait.until(elementToBeClickable(button));
    }

    public void waitAndSendKeys(WebElement field, String value) {
        wait.until(visibilityOf(field));
        clearField(field);
        field.sendKeys(value);
    }

    public Boolean URLIsOk(String shouldBeURL) {
        if (driver.getCurrentUrl().equals(shouldBeURL)) {
            return true;
        }
        return false;
    }

    // זה אמור להיות הפוך אבל משום מה הוא עובד ככה
    public void assertElementIsClickable(WebElement element) {
        wait.until(visibilityOf(element));
        Assert.assertFalse(element.isEnabled());
    }

    public void assetElementIsNotClickable(WebElement element) {
        wait.until(visibilityOf(element));
        Assert.assertTrue(element.isEnabled());
    }

    public void assertElementIsContainsClass(WebElement element, String classText){
        wait.until(visibilityOf(element));
        Assert.assertTrue(element.getAttribute("class").contains(classText));
    }

    public void clearField(WebElement field) {
        field.sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE));
    }

    public void waitInnerHtmlIsDifferentThen(WebElement element, String text) throws InterruptedException {
        wait.until(visibilityOf(element));
        while (element.getAttribute("innerHTML").contains(text)) {
            Thread.sleep(500);
        }
    }

    public void waitInnerHtmlIsContainsTo(WebElement element, String text) throws InterruptedException {
        wait.until(visibilityOf(element));
        while (!(element.getAttribute("innerHTML").contains(text))) {
            Thread.sleep(500);
        }
    }

    public void waitInnerHtmlIsContainsToOrTo(WebElement element, String text, String orText) throws InterruptedException {
        wait.until(visibilityOf(element));
        while (!(element.getAttribute("innerHTML").contains(text)) && (element.getAttribute("innerHTML").contains(orText))) {
            Thread.sleep(500);
        }
    }

    public void waitInnerHtmlIsContainsToOrToOrTo(WebElement element, String text, String orText, String orToText) throws InterruptedException {
        wait.until(visibilityOf(element));
        while (!(element.getAttribute("innerHTML").contains(text))
                && !(element.getAttribute("innerHTML").contains(orText))
                && !(element.getAttribute("innerHTML").contains(orToText))) {
            Thread.sleep(500);
        }
    }

    public SomePage LogOutFromUser() {
        LocalStorage localStorage = ((WebStorage) driver).getLocalStorage();
        localStorage.clear();
        driver.navigate().refresh();
        return new SomePage(driver, wait);
    }
}