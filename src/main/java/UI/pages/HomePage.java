package UI.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage extends PageObject{

    @FindBy(css = ".MuiButton-root.MuiButton-text.MuiButton-textPrimary.MuiButton-sizeMedium.MuiButton-textSizeMedium.MuiButtonBase-root.css-rl4qfn")
    private WebElement connectName;

    public HomePage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    public HomePage waitToClickableNewRequest() throws InterruptedException {
        super.waitInnerHtmlIsDifferentThen(connectName, "להתחברות");
        super.waitInnerHtmlIsContainsToOrToOrTo(connectName, "צהריים טובים ,נתנאל", "בוקר טוב ,נתנאל", "ערב טוב ,נתנאל");
        return this;
    }
}
