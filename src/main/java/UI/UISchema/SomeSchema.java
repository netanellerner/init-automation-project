package UI.UISchema;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import db.PersonalRequests.SomeModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import static utils.DataFaker.*;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@Data
@Accessors(fluent = true)
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(NON_NULL)
@EqualsAndHashCode
public class SomeSchema {

    private String requestReasonSubmitting;//

    private Integer req_ind;//

    public static SomeSchema returningSomeSchema() {
        return new SomeSchema()
                .requestReasonSubmitting(randomRequestReasonSubmitting())
                .req_ind(1);
    }

    public static SomeSchema createBodyFromObjects(SomeModel someModel) {
        SomeSchema someSchema = new SomeSchema()
                .requestReasonSubmitting(someModel.req_id())
                .req_ind(someModel.id());
        return someSchema;
    }
}

