package api.apiRequests.someAPI;

import api.apiSchema.uploadDocuments.SomeBody;
import framework.APIManager;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import lombok.Synchronized;
import lombok.extern.slf4j.Slf4j;

import static framework.Config.apiHost;
import static utils.JsonUtil.objectToJson;
import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;

@Slf4j
public class SomeNegativeAPI extends APIManager {
    private static final String PATH = "path/";

    @Synchronized
    public static RequestSpecification specification() {
        return APIManager.specification().baseUri(apiHost + PATH);
    }

    @Synchronized
    public static Response someTest(SomeBody someBody) {
        log.info("API: " + apiHost + PATH);

        someBody = null;

        String body = objectToJson(someBody);

        return given(specification()).contentType(JSON)
                                     .body(body)
                                     .post();
    }

}
