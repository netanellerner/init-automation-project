package api.apiRequests.someAPI;

import api.apiSchema.uploadDocuments.SomeBody;
import db.PersonalRequests.SomeDAL;
import framework.APIManager;
import framework.cleanup.CleanupService;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import lombok.Synchronized;
import lombok.extern.slf4j.Slf4j;

import static framework.Config.apiHost;
import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static utils.JsonUtil.objectToJson;

@Slf4j
public class SomePositiveAPI extends APIManager{
    private static final String PATH = "/path";

    @Synchronized
    public static RequestSpecification specification() {
        return APIManager.specification().baseUri(apiHost + PATH);
    }

    @Synchronized
    public static Response someTest(SomeBody someBody) {
        log.info("API: " + apiHost + PATH);

        String body = objectToJson(someBody);

        Response response = given(specification()).contentType(JSON)
                                                  .body(body)
                                                  .post();


        if(response.statusCode() == 201){
            Integer id = response.jsonPath().get("id");
            CleanupService.stack(() -> SomeDAL.deleteById(id));
        }
        return response;
    }
}
