package api.apiSchema.uploadDocuments;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import db.PersonalRequests.SomeModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@Data
@Accessors(fluent = true)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(NON_NULL)
@EqualsAndHashCode
public class SomeBody {

    @JsonProperty("req_id")
    private String req_id;

    @JsonProperty("id")
    private Integer id;


    public static SomeBody createSomeBodyFromObjects(SomeModel someModel) {
        SomeBody someBody = new SomeBody().req_id(someModel.req_id())
                                          .id(someModel.id());
        return someBody;
    }
}
