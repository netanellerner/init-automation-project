package framework.cleanup;

@FunctionalInterface
public interface CleanupOperation {
    void run()throws Exception;
}
