package framework.cleanup;
import lombok.SneakyThrows;
import org.testng.*;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.lang.annotation.Annotation;

public class CleanupListener implements IInvokedMethodListener, ITestListener, IClassListener, IExecutionListener {

    @Override
    @SneakyThrows(InterruptedException.class)
    public void onExecutionFinish() {
        CleanupThread thread = CleanupService.cleanupThread();

        thread.hadTestsFinished(true);

        thread.join(60000);
    }


    @Override
    public void beforeInvocation(IInvokedMethod method, ITestResult testResult) {
        for (Annotation annotation : method.getTestMethod().getConstructorOrMethod().getMethod().getAnnotations()) {
            if (BeforeTest.class.isAssignableFrom(annotation.getClass())) {
                CleanupService.setLevel(CleanupService.Level.TEST);
                return;
            }
            if (BeforeClass.class.isAssignableFrom(annotation.getClass())) {
                CleanupService.setLevel(CleanupService.Level.CLASS);
                return;
            }
            if (BeforeMethod.class.isAssignableFrom(annotation.getClass()) ||
                    Test.class.isAssignableFrom(annotation.getClass())) {
                CleanupService.setLevel(CleanupService.Level.METHOD);
                return;
            }
        }
    }

    @Override
    public void afterInvocation(IInvokedMethod method, ITestResult testResult) {
        if (method.isTestMethod()) {
            CleanupService.cleanUp(CleanupService.Level.METHOD);
        }
    }

    @Override
    public void onAfterClass(ITestClass testClass) {
        CleanupService.cleanUp(CleanupService.Level.CLASS);
    }


    @Override
    public void onFinish(ITestContext context) {
        CleanupService.cleanUp(CleanupService.Level.TEST);
    }
}