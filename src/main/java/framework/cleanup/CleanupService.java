package framework.cleanup;
import lombok.Getter;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;

import java.util.Stack;


@Slf4j
@Accessors(fluent = true)
public class CleanupService {
    private final static ThreadLocal<Stack<CleanupOperation>> classLevelStack = ThreadLocal.withInitial(Stack::new);
    private final static ThreadLocal<Stack<CleanupOperation>> methodLevelStack = ThreadLocal.withInitial(Stack::new);
    private final static ThreadLocal<Stack<CleanupOperation>> testLevelStack = ThreadLocal.withInitial(Stack::new);

    private final static ThreadLocal<Stack<CleanupOperation>> currentStack = ThreadLocal.withInitial(Stack::new);

    @Getter
    private final static CleanupThread cleanupThread = new CleanupThread();

    public static void setLevel(Level level) {
        switch (level) {
            case CLASS -> currentStack.set(classLevelStack.get());
            case METHOD -> currentStack.set(methodLevelStack.get());
            case TEST -> currentStack.set(testLevelStack.get());
        }
    }

    public static void stack(CleanupOperation CleanupOperation) {
        currentStack.get().add(CleanupOperation);
    }

    @SuppressWarnings("unchecked")
    public static void cleanUp(Level cleanupLevel) {
        Stack<CleanupOperation> stack = switch (cleanupLevel) {
            case CLASS -> classLevelStack.get();
            case METHOD -> methodLevelStack.get();
            case TEST -> testLevelStack.get();
        };

        if (stack.isEmpty()) {
            return;
        }

        cleanupThread.queueTasks((Stack<CleanupOperation>) stack.clone());
        stack.removeAllElements();
    }

    public enum Level {
        CLASS(),
        METHOD(),
        TEST()
    }
}