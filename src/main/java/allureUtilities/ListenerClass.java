package allureUtilities;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

public class ListenerClass extends TestListenerAdapter {

    @Override
    public void onTestFailure(ITestResult result) {
        Object webDriverAttribute = result.getTestContext().getAttribute("WebDriver");
        if (webDriverAttribute instanceof WebDriverManager) {
            AllureAttachment.attachScreenshot((WebDriver) webDriverAttribute);
            AllureAttachment.attachText("some text");
            AllureAttachment.getPageSource((WebDriver) webDriverAttribute);
            AllureAttachment.attachConsoleLogs((WebDriver) webDriverAttribute);
        }
    }
}