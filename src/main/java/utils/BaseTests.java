package utils;

import framework.BlackList;
import framework.ExpectedRequests;
import framework.Request;
import io.github.bonigarcia.wdm.WebDriverManager;
import net.lightbody.bmp.BrowserMobProxyServer;
import net.lightbody.bmp.core.har.Har;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.html5.LocalStorage;
import org.openqa.selenium.html5.WebStorage;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import static framework.Config.apiHost;
import static net.lightbody.bmp.proxy.CaptureType.*;

public class BaseTests {
    final String PATH = "https://app-tash-client-dev.azurewebsites.net/landing";
    //final Duration fromSeconds = Duration.ofSeconds(600);
    public WebDriver driver;
    public WebDriverWait wait;
    public static  BrowserMobProxyServer proxyServer;
    public Har har;
    //DesiredCapabilities capabilities;


    @BeforeMethod
    public void setup(ITestContext testContext, Method method) throws URISyntaxException, IOException {
        Map<String, String> mobileEmulation = new HashMap<>();
        mobileEmulation.put("deviceName", "Galaxy S5");

        ChromeOptions chromeOptions = new ChromeOptions()
                .setAcceptInsecureCerts(true)
                .setExperimentalOption("mobileEmulation", mobileEmulation);
        chromeOptions.setExperimentalOption("excludeSwitches", new String[]{"enable-automation"});


        framework.Proxy proxyMethod = method.getAnnotation(framework.Proxy.class);
        framework.Proxy proxyClass = this.getClass().getAnnotation(framework.Proxy.class);

        if ((proxyClass != null && proxyClass.enabled()) || (proxyMethod != null && proxyMethod.enabled())) {
            proxyServer=new BrowserMobProxyServer();
            proxyServer.start(18882);
            proxyServer.newHar();
            final var proxyConfig = new Proxy()
                    .setHttpProxy("127.0.0.1:18882")
                    .setSslProxy("127.0.0.1:18882")
                    .setFtpProxy("127.0.0.1:18882");
            proxyServer.enableHarCaptureTypes(
                    REQUEST_HEADERS,
                    REQUEST_COOKIES,
                    REQUEST_CONTENT,
                    REQUEST_BINARY_CONTENT,
                    RESPONSE_HEADERS,
                    RESPONSE_COOKIES,
                    RESPONSE_CONTENT,
                    RESPONSE_BINARY_CONTENT);
            chromeOptions.setProxy(proxyConfig);
            BlackList blackList = method.getAnnotation(BlackList.class);
            if (blackList != null) {
                for (Request request : blackList.value()) {
                    proxyServer.blacklistRequests(apiHost + request.route(), request.response(), request.method());
                }
            }
        }


        WebDriverManager webDriverManager = WebDriverManager
                .chromedriver()
//                 .browserInDocker()
//                .enableVnc()
//                .enableRecording()
                .capabilities(chromeOptions);
        chromeOptions.setPageLoadStrategy(PageLoadStrategy.NORMAL);

        driver = webDriverManager.create();
        testContext.setAttribute("WebDriver", this.driver);
//

//        ///////////////selenoid//////////////////////////
//        capabilities = new DesiredCapabilities();
////        capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
//        capabilities.setBrowserName("chrome");
//        capabilities.setVersion("101.0");
//        capabilities.setCapability("enableVNC", true);
//        capabilities.setCapability("enableVideo", true);
//        capabilities.setCapability("videoName", "aaa");
//        capabilities.setCapability("screenResolution", "1280x1024x24");
//        driver = new RemoteWebDriver(
//                URI.create("http://localhost:4444/wd/hub").toURL(),
//                capabilities
//        );
//        ///////////////selenoid//////////////////////////

        wait = new WebDriverWait(driver, 600);
//        Desktop.getDesktop().browse(webDriverManager.getDockerNoVncUrl().toURI());
        driver.get(PATH);
        LocalStorage localStorage = ((WebStorage) driver).getLocalStorage();
        localStorage.setItem("TEST_BYPASS_LOGIN", "true");
        localStorage.setItem("AUTOMATION_USER", "324995273");
        driver.navigate().refresh();
        driver.manage().window().maximize();
    }

    @AfterMethod
    public void tearDown(Method method) {
        ExpectedRequests expectedRequests = method.getAnnotation(ExpectedRequests.class);
//        if (expectedRequests != null) {
//            List<HarEntry> harEntries = proxyServer.getHar().getLog().getEntries();
//            for (Request request : expectedRequests.value()) {
//
//                boolean assertion = harEntries.stream()
//                                              .anyMatch(harEntry ->
//                                                      (harEntry.getRequest().getMethod().equals(request.method()) &&
//                                                              harEntry.getRequest().getUrl().equals(apiHost + request.route()) &&
//                                                              harEntry.getResponse().getStatus() == request.response()));
//                assertTrue(assertion);
//            }
//        }
        //   proxyServer.get().
        driver.quit();
    }

}