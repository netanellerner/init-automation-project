package utils;

import com.github.javafaker.Faker;

import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class DataFaker {

    private static final Faker faker = new Faker(new Locale("he"));
    private static final Faker fakerMail = new Faker();

    public static Faker faker() {
        return faker;
    }

    public static String fakePhoneNumber() {
        return faker.numerify("054#######");
    }

    public static String fakeEmail(String endingEmail) {
        if (endingEmail.equals("example")) {
            return fakerMail.letterify("???????????") + "@example.com";
        } else if (endingEmail.equals("gmail")) {
            return fakerMail.letterify("???????????") + "@gmail.com";
        } else return "the ending mail is suffix";
    }

    public static String fakeFromHour() {
        String hour = faker().number().numberBetween(0, 23) + ":" + faker().number().numberBetween(0, 60);
        String[] hourArr = hour.split(":");
        String fromHour = String.valueOf(Integer.parseInt(hourArr[0]));
        String fromMinutes = String.valueOf(Integer.parseInt(hourArr[1]));

        if (Integer.parseInt(fromHour) < 10) {
            fromHour = "0" + fromHour;
        }

        if (Integer.parseInt(fromMinutes) < 10) {
            fromMinutes = "0" + fromMinutes;
        }

        return fromHour + ":" + fromMinutes;
    }

    public static String fakeToHour(String fromHour) {
        String[] fromHourArr = fromHour.split(":");
        int fromHourForBetween = Integer.parseInt(fromHourArr[0]);
        int fromMinutesForBetween = Integer.parseInt(fromHourArr[1]);
//        String fromHourForBetweenTest = String.valueOf(Integer.parseInt(fromHourArr[0]));
//        String fromMinutesForBetweenTest = String.valueOf(Integer.parseInt(fromHourArr[1]));

//        String hour;

//        if (fromHourForBetween >= 22) {
//            hour = faker().number().numberBetween(23, 23) + ":" + faker().number().numberBetween(fromMinutesForBetween, 60);
//        } else {
        //+1                                                               +1
        String hour = faker().number().numberBetween(fromHourForBetween, 23) + ":" + faker().number().numberBetween(fromMinutesForBetween, 60);
//        }

        String[] hourArr = hour.split(":");
        String toHour = String.valueOf(Integer.parseInt(hourArr[0]));
        String toMinutes = String.valueOf(Integer.parseInt(hourArr[1]));

        if (Integer.parseInt(toHour) < 10) {
            toHour = "0" + toHour;
        }

        if (Integer.parseInt(toMinutes) < 10) {
            toMinutes = "0" + toMinutes;
        }

//        while (fromHourForBetweenTest.equals(toHour) || fromMinutesForBetweenTest.equals(toMinutes)) {
//            if (fromHourForBetween >= 22) {
//                hour = faker().number().numberBetween(23, 23) + ":" + faker().number().numberBetween(fromMinutesForBetween, 60);
//            } else {
//                hour = faker().number().numberBetween(fromHourForBetween + 1, 23) + ":" + faker().number().numberBetween(fromMinutesForBetween + 1, 60);
//            }
//
//            hourArr = hour.split(":");
//            toHour = String.valueOf(Integer.parseInt(hourArr[0]));
//            toMinutes = String.valueOf(Integer.parseInt(hourArr[1]));
//
//            if (Integer.parseInt(toHour) < 10) {
//                toHour = "0" + toHour;
//            }
//
//            if (Integer.parseInt(toMinutes) < 10) {
//                toMinutes = "0" + toMinutes;
//            }
//        }

        return toHour + ":" + toMinutes;
    }

    //todo: casting to int and then casting to string after the if
    public static String fakeNumBetweenAnd(int start, int end) {
        return String.valueOf(faker.number().numberBetween(start, end));
    }

    public static String currentDate() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        return dtf.format(java.time.LocalDate.now());
    }

    public static String currentDatePlusYear() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        String day = dtf.format(java.time.LocalDate.now()).split("-")[0];
        String month = dtf.format(java.time.LocalDate.now()).split("-")[1];
        String year = dtf.format(java.time.LocalDate.now()).split("-")[2];
        year = String.valueOf(Integer.parseInt(year) + 1);
        return day + "-" + month + "-" + year;
    }

    public static String randomDate() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        String day = dtf.format(java.time.LocalDate.now()).split("-")[0];
        String month = dtf.format(java.time.LocalDate.now()).split("-")[1];
        String year = dtf.format(java.time.LocalDate.now()).split("-")[2];
        int lastMonth = Integer.parseInt(month) - 1;

        int randomDay = Integer.parseInt(DataFaker.fakeNumBetweenAnd(1, 32));
        int randomMonth = Integer.parseInt(DataFaker.fakeNumBetweenAnd(1, 13));
        if (randomDay == 32) {
            if (randomMonth == 4 || randomMonth == 6 || randomMonth == 9 || randomMonth == 1) {
                randomDay = 30;
            }
        }
        if (randomDay > 28 && randomMonth == 2) {
            randomDay = 28;
        }

        int chooseYear = Integer.parseInt(DataFaker.fakeNumBetweenAnd(0, 2));
        int chosenYear;

        if (chooseYear == 1) {
            chosenYear = Integer.parseInt(year) + 1;
        } else chosenYear = Integer.parseInt(year);

        if (chosenYear == Integer.parseInt(year) + 1 && randomMonth == Integer.parseInt(month)) {
            randomDay = Integer.parseInt(day);
        }
        if (chosenYear == Integer.parseInt(year) && randomMonth < Integer.parseInt(month) ||
                chosenYear == Integer.parseInt(year) && randomMonth == Integer.parseInt(month) && randomDay <= Integer.parseInt(day)) {
            randomDay = Integer.parseInt(day) + 1;
            randomMonth = Integer.parseInt(month);
        }

        if (randomDay < 10) {
            day = "0" + String.valueOf(randomDay);
        }

        if (randomMonth < 10) {
            month = "0" + String.valueOf(randomMonth);
        }
        year = String.valueOf(chosenYear);

        return day + "-" + month + "-" + year;
    }

    public static String randomProduct() {
        String[] products = {"11", "13", "14", "15", "17", "19", "2", "20", "21",
                "23", "27", "3", "30", "31", "33", "34", "35", "37",
                "39", "41", "42", "44", "49", "50", "6"};
        int i = Integer.parseInt(fakeNumBetweenAnd(0, products.length - 1));
        return products[i];
    }

    public static String randomProductReason() {
        String[] option = {"02", "03"};
        int i = Integer.parseInt(fakeNumBetweenAnd(0,2));
        return option[i];
    }

    public static String randomRequestReasonSubmitting() {
        String[] option = {"02", "03", "04", "05"};
        int i = Integer.parseInt(fakeNumBetweenAnd(0,4));
        return option[i];
    }
}