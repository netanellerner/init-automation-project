package utils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static utils.DataFaker.currentDate;

public class Convertions {

    public static LocalDate stringToLocalDate(String stringDate) {
        return LocalDate.parse(stringDate, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
    }

    public static LocalDateTime getLocalDateTime() {
        return LocalDateTime.parse(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")) , DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
    }

}
