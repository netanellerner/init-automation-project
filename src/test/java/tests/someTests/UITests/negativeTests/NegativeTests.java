package tests.someTests.UITests.negativeTests;

import io.qameta.allure.*;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import utils.BaseTests;

import static io.qameta.allure.SeverityLevel.CRITICAL;

public class NegativeTests extends BaseTests {


    @BeforeMethod
    public void doSomething() {

    }

    @Link("url")
    @Owner("Netanel lerner")
    @Feature("Negative tests")
    @Story("what the test check")
    @Severity(CRITICAL)
    @Test(description = "what the result")
    void someTest() throws InterruptedException {

    }
}

