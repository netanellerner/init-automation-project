package tests.someTests.APITests.positiveTests;

import io.qameta.allure.Severity;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static io.qameta.allure.SeverityLevel.NORMAL;

@Test(groups = {"API", "Tests"})
public class positiveTests {

    @BeforeClass
    void setUp() {

    }

    @BeforeMethod
    void setup() {
    }

    @Severity(NORMAL)
    @Test(description = "Should be stats code: x")
    void someTest() throws InterruptedException {

    }

}

